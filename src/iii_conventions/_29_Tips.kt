package iii_conventions

fun todoTask29_2(): Nothing = TODO(
    """
        Task29.2.
        Support adding several time intervals to a date.
        Add an extra class for storing the time interval and the number of intervals,
        e.g. 'class RepeatedTimeInterval(val ti: TimeInterval, val n: Int)'.
        Add an extension function 'times' to TimeInterval, constructing the value of this class.
        Add an extension function 'plus' to MyDate, taking a RepeatedTimeInterval as an argument.
    """
)

class RepeatedTimeInterval(val ti: TimeInterval, val n: Int)

operator fun TimeInterval.times(n: Int): RepeatedTimeInterval = RepeatedTimeInterval(this, n)

operator fun MyDate.plus(repeatedTimeInterval: RepeatedTimeInterval) =
        this.addTimeIntervals(repeatedTimeInterval.ti, repeatedTimeInterval.n)

operator fun MyDate.plus(timeInterval: TimeInterval) =
        this.addTimeIntervals(timeInterval, 1)